# get-video-info-url

:video_camera: Get informations from a video in nodejs (dimension, duration, codec, etc...)

It works with local files and url.

---

## Install

~~~sh
npm i -S get-video-info
~~~

## Usage

~~~js
const getVideoInfo = require('get-video-info-url')

getVideoInfo('http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4').then(info => {
  console.log(info.format.duration) // => 10.007000
})
~~~
